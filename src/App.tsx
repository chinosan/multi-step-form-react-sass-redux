import { useState } from 'react'
import Sidebar from './components/Sidebar'
import Step from './components/Step'
import Step2 from './components/Step2'
import Step3 from './components/Step3'

function App() {
  const [count, setCount] = useState(0)

  return (
    
    <div className="App">
      
      {/* <!-- Sidebar start --> */}
      <Sidebar />
  {/* <!-- Sidebar end --> */}

  {/* <!-- Step 1 start --> */}
    {/* <Step /> */}
  {/* <!-- Step 1 end --> */}

  {/* <!-- Step 2 start --> */}
    {/* <Step2 /> */}
  {/* <!-- Step 2 end --> */}

  {/* <!-- Step 3 start --> */}
    <Step3 />
  {/* Pick add-ons
  
  Next Step */}

  {/* <!-- Step 3 end --> */}

  {/* <!-- Step 4 start --> */}

  {/* Finishing up
  Double-check everything looks OK before confirming. */}

  {/* <!-- Dynamically add subscription and add-on selections here --> */}

  {/* Total (per month/year)

  Go Back
  Confirm */}

  {/* <!-- Step 4 end --> */}

  {/* <!-- Step 5 start --> */}

  {/* Thank you!

  Thanks for confirming your subscription! We hope you have fun 
  using our platform. If you ever need support, please feel free 
  to email us at support@loremgaming.com. */}

  {/* <!-- Step 5 end --> */}
  <button type='button' className='btn-next' > Next Step</button>
  
  <footer className="attribution">
    Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank">Frontend Mentor</a>. 
    Coded by <a href="/https://gitlab.com/chinosan">devDonaldo</a>.
  </footer>

    </div>
  )
}

export default App
