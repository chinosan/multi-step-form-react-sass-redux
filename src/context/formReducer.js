import { createSlice } from "@reduxjs/toolkit";

const formReducer = createSlice({
    name: "steps",
    initialState: {},
    reducers: {
        setStepNumber: (state, action) =>{
            state.step = action.payload
        },
        getStep: (state) => {
            state
        }
    }
})
export const{ setStepNumber } = formReducer.actions

export default formReducer.reducer