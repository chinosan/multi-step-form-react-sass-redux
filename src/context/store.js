import { configureStore } from "@reduxjs/toolkit";
import {formReducer} from './formReducer';

export const store = configureStore({
    reducers: {
        step:  formReducer
    }
})