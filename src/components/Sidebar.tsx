import React from "react";

function Sidebar() {
	return (
		<div className="sidebar" >
			<button type='button' className="sidebar-btn" >
				<span className='btn-text'>Step</span>
				1
				<span className='btn-text'>Your info</span>
			</button>
			<button type='button' className="sidebar-btn">
				<span className='btn-text'>Step</span>
				2
				<span className='btn-text'>Select plan</span>
			</button>
			<button type='button' className="sidebar-btn">
				<span className='btn-text'>Step</span>
				3
				<span className='btn-text'>Add-ons</span>
			</button>
			<button type='button' className="sidebar-btn">
				<span className='btn-text'>Step</span>
				4
				<span className='btn-text'>Summary</span>
			</button>
		</div>
	);
}

export default Sidebar;
