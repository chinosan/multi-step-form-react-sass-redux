const Step = () => {
	return (
		<div className="step">
			<h2>Personal info</h2>
			<p>Please provide your name, email address, and phone number.</p>

			<span>Name</span>
			<input type="text" name="name" id="" placeholder="e.g. Stephen King" />

			<span>Email Address</span>
			<input
				type="email"
				name="email"
				id=""
				placeholder="e.g. stephenking@lorem.com"
			/>

			<span>Phone Number</span>
			<input
				type="number"
				name="phone"
				id=""
				placeholder="e.g. +1 234 567 890"
			/>

			{/* Next Step */}
		</div>
	);
};
export default Step;
