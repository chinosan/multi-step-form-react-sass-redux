import React from "react";

const Option = ({ ico, description, title, price, time }) => {
	return (
		<div className='step2-option'>
			<img src={ico} alt={description} />
			<div className="step2-option-rigth-container" >
				<span className='option-title'>{title}</span>
				<span className='option-price'>${price}/yr</span>
				<span className='option-time'>{time}</span>
			</div>
		</div>
	);
};

const Step2 = () => {
	const options = [
		{
			ico: "../assets/images/icon-arcade.svg",
			description: "Arcade icon",
			title: "Arcade",
			price: 90,
			time: "2 Months free",
		},
		{
			ico: "../assets/images/icon-advanced.svg",
			description: "Advanced icon",
			title: "Advanced",
			price: 20,
			time: "2 Months free",
		},
		{
			ico: "../assets/images/icon-pro.svg",
			description: "Pro icon",
			title: "Pro",
			price: 90,
			time: "2 Months free",
		},
	];
	return (
		<div className='step'>
			<h2>Select your plan</h2>
			<p>You have the option of monthly or yearly billing.</p>
            {
                options.map(op => <Option  {...op} /> )
            }
			<div className="switch-container" >
				<span>Monthly</span>
				<label className="switch">
					<input type="checkbox" className="input" />
					<span className="slider round" />
				</label>
				<span>Yearly</span>
			</div>

		</div>
	);
};

export default Step2;
